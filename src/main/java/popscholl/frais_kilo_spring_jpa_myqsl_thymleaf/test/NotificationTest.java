package popscholl.frais_kilo_spring_jpa_myqsl_thymleaf.test;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;
import popscholl.frais_kilo_spring_jpa_myqsl_thymleaf.FraisKiloSpringJpaMyqslThymleafApplication;
import popscholl.frais_kilo_spring_jpa_myqsl_thymleaf.dao.NotificationRepository;
import popscholl.frais_kilo_spring_jpa_myqsl_thymleaf.model.Notification;


@Component
public class NotificationTest implements CommandLineRunner {

    private static final Logger log = LoggerFactory.getLogger(FraisKiloSpringJpaMyqslThymleafApplication.class);

    @Autowired
    private NotificationRepository notificationRepository;

    private void information(Notification n){
        log.info(n.toString());
    }

    @Override
    public void run(String... args) throws Exception {
        //création d'un trajet
        log.info("---------------------------------------------------------------------------------------------------");
        log.info("Creation d'une notification :");
        log.info("---------------------------------------------------------------------------------------------------");
        log.info("");



    }
}
