package popscholl.frais_kilo_spring_jpa_myqsl_thymleaf.test;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;
import popscholl.frais_kilo_spring_jpa_myqsl_thymleaf.FraisKiloSpringJpaMyqslThymleafApplication;
import popscholl.frais_kilo_spring_jpa_myqsl_thymleaf.dao.UtilisateurRepository;
import popscholl.frais_kilo_spring_jpa_myqsl_thymleaf.model.Utilisateur;


@Component
public class UtilisateurTest implements CommandLineRunner {

    private static final Logger log = LoggerFactory.getLogger(FraisKiloSpringJpaMyqslThymleafApplication.class);

    @Autowired
    private UtilisateurRepository utilisateurRepository;

    private void information(Utilisateur u){
        log.info(u.toString());
    }

    @Override
    public void run(String... args) throws Exception {
        /*//création d'un utilisateur
        log.info("---------------------------------------------------------------------------------------------------");
        log.info("Creation d'un utilisateur :");
        log.info("---------------------------------------------------------------------------------------------------");
        //utilisateurRepository.save(new Utilisateur("Lefebvre", "Marie", "marielefebvre@gmail.com", "marielefebvre"));
        log.info("");

        log.info("---------------------------------------------------------------------------------------------------");
        log.info("Trouver tous les utilisateurs :");
        log.info("---------------------------------------------------------------------------------------------------");
        List<Utilisateur> utilisateur = (List<Utilisateur>) utilisateurRepository.findAll();
        utilisateur.forEach(utilisateur1 -> {
            log.info(utilisateur.toString());
        });*/


    }
}
