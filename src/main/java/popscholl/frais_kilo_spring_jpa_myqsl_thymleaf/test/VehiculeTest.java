package popscholl.frais_kilo_spring_jpa_myqsl_thymleaf.test;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;
import popscholl.frais_kilo_spring_jpa_myqsl_thymleaf.FraisKiloSpringJpaMyqslThymleafApplication;
import popscholl.frais_kilo_spring_jpa_myqsl_thymleaf.dao.UtilisateurRepository;
import popscholl.frais_kilo_spring_jpa_myqsl_thymleaf.dao.VehiculeRepository;
import popscholl.frais_kilo_spring_jpa_myqsl_thymleaf.model.Vehicule;


@Component
public class VehiculeTest implements CommandLineRunner {

    private static final Logger log = LoggerFactory.getLogger(FraisKiloSpringJpaMyqslThymleafApplication.class);

    @Autowired
    private VehiculeRepository vehiculeRepository;
    @Autowired
    private UtilisateurRepository utilisateurRepository;

    private void information(Vehicule v){
        log.info(v.toString());
    }

    @Override
    public void run(String... args) throws Exception {
        /*//création d'un utilisateur
        log.info("---------------------------------------------------------------------------------------------------");
        log.info("Creation d'un utilisateur :");
        log.info("---------------------------------------------------------------------------------------------------");
        //Utilisateur michael = utilisateurRepository.findUtilisateurById(1L);
        Utilisateur marie = utilisateurRepository.findUtilisateurById(2L);
        Vehicule vehicule = new Vehicule("Renault","Clio", 5,"MM-999-MM",marie);
        log.info(marie.toString());
        log.info(vehicule.toString());
        vehiculeRepository.save(vehicule);

        log.info("");
*/


    }
}
