package popscholl.frais_kilo_spring_jpa_myqsl_thymleaf.dao;


import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import popscholl.frais_kilo_spring_jpa_myqsl_thymleaf.model.Utilisateur;
import popscholl.frais_kilo_spring_jpa_myqsl_thymleaf.model.Vehicule;

import java.util.List;


public interface VehiculeRepository extends CrudRepository<Vehicule,Long> {
    List<Vehicule> findByUtilisateur(Utilisateur user);

}
