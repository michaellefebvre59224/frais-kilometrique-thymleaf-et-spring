package popscholl.frais_kilo_spring_jpa_myqsl_thymleaf.dao;


import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import popscholl.frais_kilo_spring_jpa_myqsl_thymleaf.model.Trajet;
import popscholl.frais_kilo_spring_jpa_myqsl_thymleaf.model.Utilisateur;

import java.util.List;


public interface TrajetRepository extends CrudRepository<Trajet, Long> {
    List<Trajet> findByArchiveAndUtilisateurEquals(Boolean valeur, Utilisateur user);

}
