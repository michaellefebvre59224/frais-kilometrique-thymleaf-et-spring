package popscholl.frais_kilo_spring_jpa_myqsl_thymleaf.controler;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import popscholl.frais_kilo_spring_jpa_myqsl_thymleaf.model.Adresse;
import popscholl.frais_kilo_spring_jpa_myqsl_thymleaf.model.Trajet;
import popscholl.frais_kilo_spring_jpa_myqsl_thymleaf.model.Utilisateur;
import popscholl.frais_kilo_spring_jpa_myqsl_thymleaf.model.Vehicule;
import popscholl.frais_kilo_spring_jpa_myqsl_thymleaf.servive.FraisKiloService;

import java.util.ArrayList;
import java.util.List;


@Controller
@SessionAttributes(value="utilisateurSession", types = {Utilisateur.class})
public class MainControler {

    @Autowired
    private FraisKiloService fraisKiloService;

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //TODO                                      CREATION DE L'UTILISATEUR EN SESSION
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    @ModelAttribute("utilisateurSession")
    public Utilisateur addMyBean1ToSessionScope() {
        return new Utilisateur();
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //TODO                                          INITIALISATION DE LA PAGE INDEX
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    @GetMapping(value =  {"/", "/index"})
    public String index(Model model){
        Utilisateur utilisateur = new Utilisateur();
        model.addAttribute("utilisateur", utilisateur);
        return "index";
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //TODO                                          LIEN MENU NAVIGATION
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    @GetMapping(value = "/addTrajetControl")
    public String toGoAddTrajet(@ModelAttribute("utilisateurSession") Utilisateur utilisateurSession, Model model){
        Trajet newTrajet = new Trajet();
        Adresse adresseDepart =new Adresse();
        Adresse adresseArrivee = new Adresse();
        model.addAttribute("newTrajet", newTrajet);
        model.addAttribute("adresseDepart", adresseDepart);
        model.addAttribute("adresseArrivee", adresseArrivee);
        return "addTrajet";
    }

    @GetMapping(value = "/home")
    public String toGoHome(@ModelAttribute("utilisateurSession") Utilisateur utilisateurSession, Model model){
        //recupération de la liste des trajets non archivés de l'utilisateur
        List<Trajet> listeTrajets = fraisKiloService.findTrajetNotArchiveByUser(utilisateurSession);
        model.addAttribute("listeTrajets", listeTrajets);

        //recupération des notifications recu
        model.addAttribute("listeNotification", utilisateurSession.getNotificationsRecu());

        //direction la page accueil
        return "home";
    }

    @GetMapping(value = "/véhiculesControl")
    public String toGoVehicules(@ModelAttribute("utilisateurSession") Utilisateur utilisateurSession, Model model){
        List<Vehicule> listeVehicule = new ArrayList<>();
        listeVehicule = fraisKiloService.findVehiculeByUtilisateur(utilisateurSession);
        model.addAttribute("listeVehicule", listeVehicule);
        return "vehicule";
    }
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //TODO                                          LIEN MENU NAVIGATION
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    @PostMapping("/Authentificate")
    public String authentificate(@ModelAttribute("utilisateur") Utilisateur utilisateur,
                                 @ModelAttribute("utilisateurSession") Utilisateur utilisateurSession,
                                 Model model){
        //verification si l'email et le mot de passe trouve un utilisateur
        Utilisateur user = fraisKiloService.authenticateUtilisateur(utilisateur.getEmail(), utilisateur.getMdp());
        if (user!= null){
            //mise en session de l'utilisateur
            utilisateurSession.setId_utilisateur(user.getId_utilisateur());
            utilisateurSession.setEmail(user.getEmail());
            utilisateurSession.setFonction(user.getFonction());
            utilisateurSession.setMdp(user.getMdp());
            utilisateurSession.setNom(user.getNom());
            utilisateurSession.setPrenom(user.getPrenom());

            //recupération de la liste des trajets non archivés de l'utilisateur
            List<Trajet> listeTrajets = fraisKiloService.findTrajetNotArchiveByUser(utilisateurSession);
            model.addAttribute("listeTrajets", listeTrajets);

            //recupération des notifications recu
            model.addAttribute("listeNotification", utilisateurSession.getNotificationsRecu());

            //direction la page accueil
            return "home";

        }else {
            //Si l'utilisateur n'est pas trouvé on repart a la page de connection
            return "index";
        }
    }

    @PostMapping("/calculTrajet")
    public String calculTrajet(@ModelAttribute("newTrajet") Trajet trajet,
                                 @ModelAttribute("utilisateurSession") Utilisateur utilisateurSession,
                                 Model model){
       return "redirect:/home";
    }

}
