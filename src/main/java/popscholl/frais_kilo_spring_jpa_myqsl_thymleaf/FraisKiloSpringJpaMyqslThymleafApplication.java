package popscholl.frais_kilo_spring_jpa_myqsl_thymleaf;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FraisKiloSpringJpaMyqslThymleafApplication {

    public static void main(String[] args) {
        SpringApplication.run(FraisKiloSpringJpaMyqslThymleafApplication.class, args);
    }

}
