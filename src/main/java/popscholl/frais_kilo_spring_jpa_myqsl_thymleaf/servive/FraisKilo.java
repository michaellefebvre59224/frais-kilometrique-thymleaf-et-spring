package popscholl.frais_kilo_spring_jpa_myqsl_thymleaf.servive;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import popscholl.frais_kilo_spring_jpa_myqsl_thymleaf.dao.*;
import popscholl.frais_kilo_spring_jpa_myqsl_thymleaf.model.Trajet;
import popscholl.frais_kilo_spring_jpa_myqsl_thymleaf.model.Utilisateur;
import popscholl.frais_kilo_spring_jpa_myqsl_thymleaf.model.Vehicule;


import javax.transaction.Transactional;
import java.util.List;


@Service
@Transactional
public class FraisKilo implements FraisKiloService {

    @Autowired
    private AdresseRepository adresseRepository;

    @Autowired
    private NotificationRepository notificationRepository;

    @Autowired
    private TrajetRepository trajetRepository;

    @Autowired
    private UtilisateurRepository utilisateurRepository;

    @Autowired
    private VehiculeRepository vehiculeRepository;

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //TODO                                            METHODE UTILISATEUR
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    @Override
    public Utilisateur authenticateUtilisateur(String email, String mdp) {
        return utilisateurRepository.findByEmailAndMdp(email, mdp);

    }

    @Override
    public List<Trajet> findTrajetNotArchiveByUser(Utilisateur user) {
        return trajetRepository.findByArchiveAndUtilisateurEquals(false,user);
    }

    @Override
    public List<Vehicule> findVehiculeByUtilisateur(Utilisateur user) {
        return vehiculeRepository.findByUtilisateur(user);
    }


}
