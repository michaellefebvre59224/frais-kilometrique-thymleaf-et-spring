package popscholl.frais_kilo_spring_jpa_myqsl_thymleaf.servive;


import popscholl.frais_kilo_spring_jpa_myqsl_thymleaf.model.Trajet;
import popscholl.frais_kilo_spring_jpa_myqsl_thymleaf.model.Utilisateur;
import popscholl.frais_kilo_spring_jpa_myqsl_thymleaf.model.Vehicule;

import java.util.List;

public interface FraisKiloService {
    Utilisateur authenticateUtilisateur(String eail, String mdp);

    List<Trajet> findTrajetNotArchiveByUser(Utilisateur user);

    List<Vehicule> findVehiculeByUtilisateur(Utilisateur user);
}
