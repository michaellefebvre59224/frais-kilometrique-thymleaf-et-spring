package popscholl.frais_kilo_spring_jpa_myqsl_thymleaf.model;

import lombok.Data;
import org.springframework.boot.autoconfigure.domain.EntityScan;

import javax.persistence.*;
import java.util.Objects;


@Data
@Entity
@Table(name = "adresse")
public class Adresse {
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //TODO                                            VARIABLE D'INSTANCE
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id_adresse;

    private String numero;
    private String type_rue;
    private String nom_rue;
    private String code_postal;
    private String ville;
    private String pays;
    private String region;
    private String coordonnees;

    @ManyToOne
    @JoinColumn(name="id_utilisateur")
    private Utilisateur utilisateur;

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //TODO                                            CONSTRUCTEUR
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public Adresse() {
    }

    public Adresse(String numero, String type_rue, String nom_rue,
                   String code_postal, String ville, String pays, String region, String coordonnees) {
        this.numero = numero.toUpperCase();
        this.type_rue = type_rue.toUpperCase();
        this.nom_rue = nom_rue.toUpperCase();
        this.code_postal = code_postal.toUpperCase();
        this.ville = ville.toUpperCase();
        this.pays = pays.toUpperCase();
        this.region = region.toUpperCase();
        this.coordonnees = coordonnees;
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //TODO                                            TOSTRING
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    @Override
    public String toString() {
        return "Adresse{" +
                "id_adresse=" + id_adresse +
                ", numero='" + numero + '\'' +
                ", type_rue='" + type_rue + '\'' +
                ", nom_rue='" + nom_rue + '\'' +
                ", code_postal='" + code_postal + '\'' +
                ", ville='" + ville + '\'' +
                ", pays='" + pays + '\'' +
                ", region='" + region + '\'' +
                ", coordonnees='" + coordonnees + '\'' +
                ", utilisateur=" + utilisateur +
                '}';
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //TODO                                            EQUALS ET HASH
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Adresse)) return false;
        Adresse adresse = (Adresse) o;
        return coordonnees.equals(adresse.coordonnees) &&
                utilisateur.equals(adresse.utilisateur);
    }

    @Override
    public int hashCode() {
        return Objects.hash(coordonnees, utilisateur);
    }
}
