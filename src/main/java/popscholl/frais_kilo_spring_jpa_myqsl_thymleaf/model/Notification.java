package popscholl.frais_kilo_spring_jpa_myqsl_thymleaf.model;

import lombok.Data;

import javax.persistence.*;
import java.util.Date;

@Data
@Entity
@Table(name = "notification")
public class Notification {
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //TODO                                            VARIABLE D'INSTANCE
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id_notification;

    private Date date_notification;
    private String commentaire;
    private Boolean archive;

    @ManyToOne
    @JoinColumn(name = "id_utilisateur_destinataire")
    private Utilisateur utilisateur_destinataire;

    @ManyToOne
    @JoinColumn(name = "id_utilisateur_destinateur")
    private Utilisateur utilisateur_destinateur;

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //TODO                                            CONSTRUCTEUR
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public Notification() {
    }

    public Notification(Date date_notification, String commentaire, Boolean archive,
                        Utilisateur utilisateur_destinataire, Utilisateur utilisateur_destinateur) {
        this.date_notification = date_notification;
        this.commentaire = commentaire;
        this.archive = archive;
        this.utilisateur_destinataire = utilisateur_destinataire;
        this.utilisateur_destinateur = utilisateur_destinateur;
    }

}

