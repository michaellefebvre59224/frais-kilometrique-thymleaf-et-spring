package popscholl.frais_kilo_spring_jpa_myqsl_thymleaf.model;

import lombok.Data;

import javax.persistence.*;
import java.util.Objects;

@Data
@Entity
@Table(name = "vehicule")
public class Vehicule {
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //TODO                                            VARIABLE D'INSTANCE
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id_vehicule;

    private String marque;
    private String modele;
    private Integer puissance;
    private String immat;

    @ManyToOne
    @JoinColumn(name="id_utilisateur")
    private Utilisateur utilisateur;

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //TODO                                            CONSTRUCTEUR
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public Vehicule() {
    }

    public Vehicule(String marque, String modele, Integer puissance, String immat, Utilisateur utilisateur) {
        this.marque= marque.toUpperCase();
        this.modele=modele.toUpperCase();
        this.puissance = puissance;
        this.immat=immat.toUpperCase();
        this.utilisateur = utilisateur;
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //TODO                                            TOSTRING
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    @Override
    public String toString() {
        return "Vehicule{" +
                "id_vehicule=" + id_vehicule +
                ", marque='" + marque + '\'' +
                ", modele='" + modele + '\'' +
                ", puissance=" + puissance +
                ", immat='" + immat + '\'' +
                ", utilisateur=" + utilisateur +
                '}';
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //TODO                                            EQUALS ET HASH
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Vehicule)) return false;
        Vehicule vehicule = (Vehicule) o;
        return Objects.equals(immat, vehicule.immat);
    }

    @Override
    public int hashCode() {
        return Objects.hash(immat);
    }
}
