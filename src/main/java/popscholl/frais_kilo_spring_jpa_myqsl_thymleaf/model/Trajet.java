package popscholl.frais_kilo_spring_jpa_myqsl_thymleaf.model;

import lombok.Data;

import javax.persistence.*;
import java.util.Date;

@Data
@Entity
@Table(name = "trajet")
public class Trajet {
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //TODO                                            VARIABLE D'INSTANCE
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id_trajet;

    private Date date_trajet;
    private String route;
    private Double distance;
    private Boolean archive;

    @ManyToOne
    @JoinColumn(name = "id_utilisateur")
    Utilisateur utilisateur;

    @ManyToOne
    @JoinColumn(name = "id_adresse_dep")
    private Adresse adresse_depart;

    @ManyToOne
    @JoinColumn(name = "id_adresse_arr")
    private Adresse adresse_arrivee;

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //TODO                                            CONSTRUCTEUR
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public Trajet() {
    }

    public Trajet(Date date_trajet, String route, Double distance, Adresse adresse_depart,
                  Adresse adresse_arrivee, Boolean archive) {
        this.date_trajet = date_trajet;
        this.route = route;
        this.distance = distance;
        this.adresse_depart = adresse_depart;
        this.adresse_arrivee = adresse_arrivee;
        this.archive = archive;
    }
}
