package popscholl.frais_kilo_spring_jpa_myqsl_thymleaf.model;

import lombok.Data;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Data
@Entity
@Table(name = "utilisateur")
public class Utilisateur {
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //TODO                                            VARIABLE D'INSTANCE
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id_utilisateur;

    private String nom;
    private String prenom;
    private String email;
    private String mdp;
    private String fonction;

    @OneToMany(mappedBy = "utilisateur")
    private List<Trajet> trajets = new ArrayList<>();

    @OneToMany(mappedBy = "utilisateur_destinataire")
    private List<Notification> notificationsRecu = new ArrayList<>();

    @OneToMany(mappedBy = "utilisateur_destinateur")
    private List<Notification> notificationEnvoiye = new ArrayList<>();


    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //TODO                                            GETTER SETTER
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public Long getId_utilisateur() {
        return id_utilisateur;
    }

    public void setId_utilisateur(Long id_utilisateur) {
        this.id_utilisateur = id_utilisateur;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getMdp() {
        return mdp;
    }

    public void setMdp(String mdp) {
        this.mdp = mdp;
    }

    public String getFonction() {
        return fonction;
    }

    public void setFonction(String fonction) {
        this.fonction = fonction;
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //TODO                                            CONSTRUCTEUR
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public Utilisateur() {
    }

    public Utilisateur(String nom, String prenom, String email, String mdp) {
        this.nom=nom.toUpperCase();
        this.prenom=prenom.toUpperCase();
        this.email=email.toLowerCase();
        this.mdp=mdp;
        this.fonction="UTILISATEUR";
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //TODO                                            TOSTRING
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    @Override
    public String toString() {
        return "Utilisateur{" +
                "id_utilisateur=" + id_utilisateur +
                ", nom='" + nom + '\'' +
                ", prenom='" + prenom + '\'' +
                ", email='" + email + '\'' +
                ", mdp='" + mdp + '\'' +
                ", fonction='" + fonction + '\'' +
                '}';
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //TODO                                            EQUAL ET HASH
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Utilisateur)) return false;
        Utilisateur that = (Utilisateur) o;
        return email.equals(that.email);
    }

    @Override
    public int hashCode() {
        return Objects.hash(email);
    }
}
